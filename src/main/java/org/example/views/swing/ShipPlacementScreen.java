package org.example.views.swing;

import org.example.InvalidShipException;
import org.example.models.Field;
import org.example.models.Point;
import org.example.models.Ship;
import org.example.models.ShipDirection;
import org.example.views.IScreen;
import org.example.views.IShipPlacementScreen;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ShipPlacementScreen extends JFrame implements IShipPlacementScreen {
    private Field field;
    private Runnable randomPlacementHandler = null;
    private FieldView fieldView;

    public ShipPlacementScreen(Field field) {
        super("Морской бой.");
        this.field = field;
        setSize(800, 600);
        setMinimumSize(new Dimension(300, 200));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); //can be replaced
        setLocationRelativeTo(null);

        setUpUI();
    }

    private void setUpUI() {
        JLabel caption = new JLabel("Расстановка кораблей", SwingConstants.LEFT);
        JButton randomBtn = new JButton("Случайно");
//        JButton fourDecksBtn = new JButton("4x-палубный - 1");
//        JButton threeDecksBtn = new JButton("3x-палубный - 2");
//        JButton twoDecksBtn = new JButton("2x-палубный - 3");
//        JButton oneDeckBtn = new JButton("1x-палубный - 4");

        JComponent fourDecksBtn = new ShipOKButton(4);
        JComponent threeDecksBtn = new ShipOKButton(4);
        JComponent twoDecksBtn = new ShipOKButton(4);
        JComponent oneDeckBtn = new ShipOKButton(4);

        JButton nextBtn = new JButton("Далее");
        JPanel btnPanel = new JPanel();
        JPanel contentPanel = new JPanel();
        JPanel nextPanel = new JPanel();
        JPanel windowPanel = new JPanel();
        fieldView = new OwnerFieldView(field);
        //todo: ShipOKButton

        randomBtn.addActionListener(this::handleRandomClicked);
//        fourDecksBtn.addActionListener(this::handleFourDecksClicked);
//        threeDecksBtn.addActionListener(this::handleThreeDecksClicked);
//        twoDecksBtn.addActionListener(this::handleTwoDecksClicked);
//        oneDeckBtn.addActionListener(this::handleOneDeckClicked);
        nextBtn.addActionListener(this::handleNextClicked);


        caption.setFont(new Font("Arial", Font.BOLD, 36));

        btnPanel.setLayout(new BoxLayout(btnPanel, BoxLayout.PAGE_AXIS));
        btnPanel.add(randomBtn);
        btnPanel.add(fourDecksBtn);
        btnPanel.add(threeDecksBtn);
        btnPanel.add(twoDecksBtn);
        btnPanel.add(oneDeckBtn);
        btnPanel.add(Box.createVerticalGlue());

        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.LINE_AXIS));
        contentPanel.add(fieldView);
        contentPanel.add(btnPanel);

        nextPanel.setLayout(new BoxLayout(nextPanel, BoxLayout.LINE_AXIS));
        nextPanel.add(Box.createHorizontalGlue());
        nextPanel.add(nextBtn);

        windowPanel.setLayout(new BoxLayout(windowPanel, BoxLayout.PAGE_AXIS));
        windowPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        windowPanel.add(caption);
        windowPanel.add(contentPanel);
        windowPanel.add(nextPanel);

//        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
//        getContentPane().
//        add(caption);
//        add(contentPanel);
//        add(nextPanel);

        setContentPane(windowPanel);

        //pack();
    }

    private void handleRandomClicked(ActionEvent actionEvent) {
        field.clear();
        if (randomPlacementHandler != null) randomPlacementHandler.run();
        fieldView.repaint();
    }

    private void handleFourDecksClicked(ActionEvent actionEvent) {

    }

    private void handleThreeDecksClicked(ActionEvent actionEvent) {

    }

    private void handleTwoDecksClicked(ActionEvent actionEvent) {

    }

    private void handleOneDeckClicked(ActionEvent actionEvent) {

    }

    private void handleNextClicked(ActionEvent actionEvent) {

    }



    @Override
    public void display() {
        setVisible(true);
    }

    @Override
    public void setRandomPlacementHandler(Runnable randomPlacementHandler) {
        this.randomPlacementHandler = randomPlacementHandler;
    }

    public static void main(String[] args) {
        try {
            new ShipPlacementScreen(new Field()).setVisible(true);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}

//todo: homeTask 26.08.21
/*
 1. добавить обработчики кнопок
 2. сделать филдВью квадратным
 */
