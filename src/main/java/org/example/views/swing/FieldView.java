package org.example.views.swing;

import org.example.views.IScreen;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;

public abstract class FieldView extends JComponent implements IScreen {

    public FieldView() {
        //todo: createFont
        Font font = new Font("Arial", Font.PLAIN, 18);
//        Font font = getFont().deriveFont(24f);
//        Font font = new Font();
        setFont(font);
    }

    @Override
    public void display() {
        setVisible(true);
    }

    @Override
    protected void paintComponent(Graphics g) {
        Dimension dimension = getSize();
        int width = dimension.width;
        int height = dimension.height;
        int min = Math.min(height, width);
        int cellSize = min / 11;
        width = height = cellSize * 11;

        for (int i = 0; i <= 11; i++) {
            int x = cellSize * i;
            g.drawLine(x, 0, x, height); //вертикальные линии решетки

            int y = cellSize * i;
            g.drawLine(0, y, width, y); //горизонтальные линии решетки
        }

        for (int i = 1; i <= 10; i++) {
            //цифры слева
            int y = cellSize * i + cellSize / 2 + 8;
            int x = cellSize / 2 - 5;
            g.drawString("" + i, x, y);

            //буквы сверху
            y = cellSize / 2 + 8;
            x = cellSize * i + cellSize / 2 -5;
            char letter = (char) ('A' + i - 1);
            g.drawString(String.format("%s", letter), x, y);
        }

        //отображение содержимого ячейки
        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 10; j++) {
                int x = cellSize * j;
                int y = cellSize * i;
                CellRect cellRect = new CellRect(x, y, cellSize, cellSize);
                displayCell((Graphics2D) g, i, j, cellRect);
            }
        }
    }

    @Override
    protected void processMouseEvent(MouseEvent e) {
        super.processMouseEvent(e);
    }

    @Override
    protected void processMouseMotionEvent(MouseEvent e) {
        super.processMouseMotionEvent(e);
    }

    public abstract void displayCell(Graphics2D g, int row, int column, CellRect cellRect);

    @Override
    public Dimension getMinimumSize() {
        return new Dimension(300, 300);
    }
}
