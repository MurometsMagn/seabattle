package org.example.views.swing;

import org.example.models.Cell;
import org.example.models.Field;

import java.awt.*;

public class OpponentFieldView extends FieldView{
    private Field field;

    public OpponentFieldView(Field field) {
        this.field = field;
    }

    @Override
    public void displayCell(Graphics2D g, int row, int column, CellRect cellRect) {
        Cell cell = field.getCell(row, column);
        if (cell.isShot()) {
            Stroke oldStroke = g.getStroke();
            g.setStroke(new BasicStroke(3));
            if (cell.isShip()) {
                g.drawRect(cellRect.getX(), cellRect.getY(), cellRect.getWidth(), cellRect.getHeight());
                g.drawLine(cellRect.getX(), cellRect.getY(),
                        cellRect.getX() + cellRect.getWidth(), cellRect.getY() + cellRect.getHeight());
                g.drawLine(cellRect.getX() + cellRect.getWidth(), cellRect.getY(),
                        cellRect.getX(), cellRect.getY() + cellRect.getHeight());
            } else {
                g.drawOval(cellRect.getX() + cellRect.getWidth() / 2, cellRect.getY() + cellRect.getHeight() / 2,
                        3, 3);
            }

            g.setStroke(oldStroke);
        }
    }
}
