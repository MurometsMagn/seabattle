package org.example.views.swing;

import org.example.views.IScreen;

import javax.swing.*;
import java.awt.*;

public class GameResultScreen extends JFrame implements IScreen {

    public GameResultScreen(boolean isWinner) {
        super("Морской бой.");
        String result = isWinner ? "Вы победили!!" : "Вы проиграли..";
        setSize(800, 600);
        setMinimumSize(new Dimension(300, 200));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); //can be replaced
        setLocationRelativeTo(null);

        setUpUI(result);
    }

    @Override
    public void display() {
        setVisible(true);
    }

    private void setUpUI(String text) {
        JLabel displayText = new JLabel(text);
        add(displayText);
    }

    public static void main(String[] args) {
        try {
            new GameResultScreen(true).display();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}

//todo: центрировать текст

// SeaBattleField
