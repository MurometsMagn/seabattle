package org.example.views.swing;

import org.example.models.Point;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.Consumer;

public class BattleScreen extends JFrame {
    private JButton button1 = new JButton("1");
    private JButton button2 = new JButton("2");
    //private Point point = null;
    private Consumer<Point> listener = null;

    public BattleScreen() {
        BoxLayout layout = new BoxLayout(this, BoxLayout.X_AXIS);

        add(button1);
        add(button2);

        button1.setEnabled(false);
        button2.setEnabled(false);

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Point point = new Point(0, 1);
                button1.setEnabled(false);
                button2.setEnabled(false);
                notifyListener(point);
            }
        });

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Point point = new Point(0, 2);
                button1.setEnabled(false);
                button2.setEnabled(false);
                notifyListener(point);
            }
        });
    }

    public void setListener(Consumer<Point> listener) {
        this.listener = listener;
    }

    public void askPlayerStep() {
        button1.setEnabled(true);
        button2.setEnabled(true);
    }

    private void notifyListener(Point point) {
        if (listener != null) {
            listener.accept(point);
        }
    }
}
