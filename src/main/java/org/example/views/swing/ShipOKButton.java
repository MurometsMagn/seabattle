package org.example.views.swing;

import org.example.models.ShipDirection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;

public class ShipOKButton extends JComponent {
    private int shipSize; //1-4
    //private int squareHeight;
    private ShipDirection shipDirection;
    private Runnable clickListener = null;

    public ShipOKButton(int shipSize, ShipDirection shipDirection) {
        this.shipSize = shipSize;
        //this.squareHeight = squareHeight;
        this.shipDirection = shipDirection;
        setPreferredSize(new Dimension(4 * 32, 4 * 32));
    }

    public ShipOKButton(int shipSize) {
        new ShipOKButton(shipSize, ShipDirection.Horizontal);
    }

    public void rotate() {
        if (shipDirection == ShipDirection.Horizontal) {
            shipDirection = ShipDirection.Vertical;
        } else {
            shipDirection = ShipDirection.Horizontal;
        }
    }

    public void setClickListener(Runnable clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    protected void processMouseMotionEvent(MouseEvent e) {
        super.processMouseMotionEvent(e);
    }

    @Override
    protected void processMouseEvent(MouseEvent e) {
        super.processMouseEvent(e);
    }

    @Override
    protected void paintComponent(Graphics g) {
        Dimension dimension = getSize();
        int cellSize = dimension.width / 4;
        int width = cellSize, height = cellSize;
        if (shipDirection == ShipDirection.Horizontal) {
            width *= shipSize;
        } else {
            height *= shipSize;
        }
        g.drawRect(this.getX(), this.getY(), width, height);

        for (int i = 1; i < shipSize; i++) {
            if (shipDirection == ShipDirection.Horizontal) {
                g.drawLine(this.getX() + i * cellSize, this.getY(),
                        this.getX() + i * cellSize, this.getY()  + cellSize);
            } else {
                g.drawLine(this.getX(), this.getY() + i * cellSize,
                        this.getX() + cellSize, this.getY()  + i * cellSize);
            }
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.add(new ShipOKButton(4, ShipDirection.Vertical));
        frame.setSize(800, 800);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
