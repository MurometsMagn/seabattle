package org.example.views;

import org.example.models.Field;

public interface IViewFactory {
    IScreen createOwnerFieldView(Field field);
    IScreen createOpponentFieldView(Field field);

    IShipPlacementScreen createShipPlacementScreen(Field field);

    IGameScreen createGameScreen(IScreen ownerFieldView, IScreen opponentFieldView);

    IScreen createGameResultScreen(boolean isWinner);
}
