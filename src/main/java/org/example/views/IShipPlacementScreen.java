package org.example.views;

public interface IShipPlacementScreen extends IScreen {
    void setRandomPlacementHandler(Runnable randomPlacementHandler);
}
