package org.example.views;

import org.example.models.Point;

import java.util.function.Consumer;

/**
 * интерфейс определяет поведение, а не реализацию.
 * Если мы пишем реализацию в интерфейсе - значит мы делаем что-то не так.
 */
public interface IGameScreen extends IScreen{
    void setListener(Consumer<Point> listener);

    void askPlayerStep();

    void notifyPlayerShot(Point shot, boolean hit, boolean sunk);
    void notifyCompShot(Point shot, boolean hit, boolean sunk);
}
