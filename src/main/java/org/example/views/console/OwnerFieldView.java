package org.example.views.console;

import org.example.InvalidShipException;
import org.example.models.Cell;
import org.example.models.Field;
import org.example.models.Ship;
import org.example.views.IScreen;

public class OwnerFieldView implements IScreen {
    private final Field field;

    public OwnerFieldView(Field field) {
        this.field = field;
    }

    public void display() {
        System.out.println("   A B C D E F G H I J");

        for (int row = 1; row <= 10; row++) {
            System.out.printf("%-3d", row);
            for (int col = 1; col <= 10; col++) {
                Cell cell = field.getCell(row, col);
                CellContent cellContent = CellContent.fromCell(cell);
                String string = cellContent.getMarker();
                System.out.print(string);
            }
            System.out.println();
        }
    }

    public void addShip(Ship ship) throws InvalidShipException {
        field.addShip(ship);
    }
}
