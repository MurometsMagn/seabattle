package org.example.views.console;

/*
   A B C D E F G H I J
1    [][]    []
2            []
3            []
4
5
6
7
8
9
10

Разместите 4-палубный корабль
Укажите его направление: X - горизонтальное, Y - вертикальное
X
Введите координаты левой верхней точки (напр. F8)
C2
Невозможно разместить здесь корабль, попробуйте еще раз.
Разместите 4-палубный корабль
...
 */

import org.example.InvalidCoordinateException;
import org.example.InvalidShipException;
import org.example.models.Field;
import org.example.models.Point;
import org.example.models.Ship;
import org.example.models.ShipDirection;
import org.example.views.IShipPlacementScreen;

import java.util.Locale;
import java.util.Scanner;

//размещение только кораблей игрока
public class ShipPlacementScreen implements IShipPlacementScreen {
    private final OwnerFieldView fieldView;
    private Scanner scanner = new Scanner(System.in);
    private Runnable randomPlacementHandler = null;

    public ShipPlacementScreen(Field field) {
        fieldView = new OwnerFieldView(field);
    }

    public void setRandomPlacementHandler(Runnable randomPlacementHandler) {
        this.randomPlacementHandler = randomPlacementHandler;
    }

    public void display() {
        System.out.println("Расставить корабли автоматически? Y/N");
        String answer = scanner.nextLine().toLowerCase(Locale.ROOT);
        if (answer.equals("y") || answer.equals("yes")) {
            if (randomPlacementHandler != null) randomPlacementHandler.run();
        } else {
            placeShipsByPlayer();
        }
    }

    private void placeShipsByPlayer() {
        int[] deckSizes = {4, 3, 3, 2, 2, 2, 1, 1, 1, 1};
        for (int deckSize : deckSizes) {
            while (true) {
                fieldView.display();
                Ship ship = askShipPlacement(deckSize);
                try {
                    fieldView.addShip(ship);
                    break;
                } catch (InvalidShipException e) {
                    System.out.println("Нельзя разместить здесь этот корабль");
                    //i--; //чтобы не делать еще один цикл (внутренний бесконечный)
                }
            }
        }
    }

    private Ship askShipPlacement(int shipSize) {
        System.out.println("Разместите " + shipSize + "-палубный корабль");
        ShipDirection shipDirection = shipSize > 1 ? askShipDirection() : ShipDirection.Horizontal;
        Point point = askShipPoint();
        return new Ship(point, shipDirection, shipSize);
    }

    private ShipDirection askShipDirection() {
        while (true) {
            System.out.println("Укажите его направление: X - горизонтальное, Y - вертикальное");
            String dir = scanner.nextLine();
            switch (dir) {
                case "X", "x" -> {
                    return ShipDirection.Horizontal;
                }
                case "Y", "y" -> {
                    return ShipDirection.Vertical;
                }
                default -> System.out.println("ошибка написания, попробуйте еще раз:");
            }
        }
    }

    private Point askShipPoint() {
        /*
        запрос к пользователю на левВерхТОчку
        разбор введенной строки
        возвращает пойнт
         */
        while (true) {
            System.out.println("Введите координаты левой верхней точки (напр. F8)");
            String pointLetters = scanner.nextLine();

            try {
                return Point.fromString(pointLetters);
            } catch (InvalidCoordinateException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}

//разбить метод парсШипПойнт на несколько до 15 строк
//преобразование строки и преобразование столбца - в отдельные методы