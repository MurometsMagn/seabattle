package org.example.views.console;

import org.example.models.Cell;

public enum CellContent {
    //Empty является единственным экземпляром наследника CellContent
    Empty {
        @Override
        public String getMarker() {
            return "  ";
        }
    },
    Ship {
        @Override
        public String getMarker() {
            return "[]";
        }
    },
    ShotShip {
        @Override
        public String getMarker() {
            return "><";
        }
    },
    Miss {
        @Override
        public String getMarker() {
            return ". ";
        }
    };

    public abstract String getMarker();

    public static CellContent fromCell(Cell cell) {
        if (cell.isShip() && cell.isShot()) return ShotShip;
        if (cell.isShip() && !cell.isShot()) return Ship;
        if (!cell.isShip() && cell.isShot()) return Miss;
        return Empty;
    }
}
