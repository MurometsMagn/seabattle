package org.example.views.console;

import org.example.models.Field;
import org.example.views.IGameScreen;
import org.example.views.IScreen;
import org.example.views.IShipPlacementScreen;
import org.example.views.IViewFactory;

public class ViewFactory implements IViewFactory {
    @Override
    public IScreen createOwnerFieldView(Field field) {
        return new OwnerFieldView(field);
    }

    @Override
    public IScreen createOpponentFieldView(Field field) {
        return new OpponentFieldView(field);
    }

    @Override
    public IShipPlacementScreen createShipPlacementScreen(Field field) {
        return new ShipPlacementScreen(field);
    }

    @Override
    public IGameScreen createGameScreen(IScreen ownerFieldView, IScreen opponentFieldView) {
        return new GameScreen(ownerFieldView, opponentFieldView);
    }

    @Override
    public IScreen createGameResultScreen(boolean isWinner) {
        return new GameResultScreen(isWinner);
    }
}
