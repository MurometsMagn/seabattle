package org.example.views.console;

import org.example.InvalidCoordinateException;
import org.example.models.Point;
import org.example.views.IGameScreen;
import org.example.views.IScreen;

import java.util.Scanner;
import java.util.function.Consumer;

public class GameScreen implements IGameScreen {
    private IScreen ownerFieldView;
    private IScreen opponentFieldView;
    private Scanner scanner = new Scanner(System.in);
    private Consumer<Point> listener = null;

    public GameScreen(IScreen ownerFieldView, IScreen opponentFieldView) {
        this.ownerFieldView = ownerFieldView;
        this.opponentFieldView = opponentFieldView;
    }

    public void setListener(Consumer<Point> listener) {
        this.listener = listener;
    }

    private void notifyListener(Point point) {
        if (listener != null) {
            listener.accept(point);
        }
    }

    public void display() {
        ownerFieldView.display();
        opponentFieldView.display();
    }

    public void askPlayerStep() {
        display();
        while (true) {
            System.out.println("Введите координаты выстрела, например F4");
            String coords = scanner.nextLine();
            try {
                Point point = Point.fromString(coords);
                notifyListener(point);
                break;
            } catch (InvalidCoordinateException e) {
                System.out.println("Неверно введены координаты, попробуйте еще раз!!");
            }
        }
    }

    public void notifyPlayerShot(Point shot, boolean hit, boolean sunk) {
        notifyShot(shot, hit, sunk, "игрока");
    }

    public void notifyCompShot(Point shot, boolean hit, boolean sunk) {
        notifyShot(shot, hit, sunk, "компьютера");
    }

    private void notifyShot(Point shot, boolean hit, boolean sunk, String shooter) {
        String result;
        if (!hit) result = "мимо";
        else if (sunk) result = "убит";
        else result = "ранен";

        System.out.println("Ход " + shooter + ": " + shot + ": " + result);
    }


}

//todo: hometask 3.08.21
/*
компБаг: комп промахивается, но думает, что попал
 1.проверить попадание точки в прямоугольник
 2.проверить генерацию случайной точки (генерация с нуля или с единицы)
 3.проверить расстановку экземпляров Cell-ов корабля (индексация с ноля или с единицы)
 4.проверить алгоритм компьютера при нуле попаданий
 */

//todo: hometask 5.08.21

/**
 * 1. добавить в отображение хода игрока инфу о том, кто стрелял
 * 2. исправить перевод экземпляра Пойнт в строку.
 */
