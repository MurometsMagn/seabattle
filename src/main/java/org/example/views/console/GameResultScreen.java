package org.example.views.console;

import org.example.views.IScreen;

public class GameResultScreen implements IScreen {
    private final boolean isWinner;

    public GameResultScreen(boolean isWinner) {
        this.isWinner = isWinner;
    }

    public void display() {
        if (isWinner) {
            System.out.println("Вы победили!!");
        } else {
            System.out.println("Вы проиграли..");
        }
    }
}
