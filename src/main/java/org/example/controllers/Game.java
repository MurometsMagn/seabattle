package org.example.controllers;

import org.example.models.Field;
import org.example.views.IViewFactory;

public class Game {
    private GameState state;
    private boolean isRunning = false;
    private Field player1Field = new Field();
    private Field player2Field = new Field();

    public Game(IViewFactory factory) {
        state = new ShipPlacementState(this, factory);
    }

    public Field getPlayer1Field() {
        return player1Field;
    }

    public Field getPlayer2Field() {
        return player2Field;
    }

    public void setState(GameState state) {
        this.state = state;
    }

    public void stop() {
        isRunning = false;
    }

    public void run() {
        isRunning = true;
        while (isRunning) {
            state.run();
        }
    }
}
