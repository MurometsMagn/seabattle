package org.example.controllers;

import org.example.models.*;
import org.example.views.IGameScreen;
import org.example.views.IScreen;
import org.example.views.IViewFactory;

public class BattleState implements GameState {
    private final Game game;
    private final IScreen ownerFieldView;
    private final IScreen opponentFieldView;
    private final IGameScreen gameScreen;
    private int shotTheSameShipCount = 0;
    private Point lastPoint;
    private Point firstPoint;
    private ShootDirection direction = ShootDirection.Right;
    private IViewFactory factory;

    public BattleState(Game game, IViewFactory factory) {
        this.game = game;
        ownerFieldView = factory.createOwnerFieldView(game.getPlayer1Field());
        opponentFieldView = factory.createOpponentFieldView(game.getPlayer2Field());
        gameScreen = factory.createGameScreen(ownerFieldView, opponentFieldView);
        this.factory = factory;
        gameScreen.setListener(this::handleUserChoice);
    }

    private void handleUserChoice(Point point) {
        //выстрелить в поле оппонента по выбранным точкам
        Field player2Field = game.getPlayer2Field();
        player2Field.shootAt(point);
        Cell cell = player2Field.getCell(point.row, point.column);
        if (cell.isShip()) {
            Ship shotShip = player2Field.findShip(point);
            gameScreen.notifyPlayerShot(point, true, shotShip.isSunk());
            if (!checkWinner()) gameScreen.askPlayerStep();
        } else {
            gameScreen.notifyPlayerShot(point, false, false);
            if (!checkWinner()) compTurn();
        }
    }

    private void compTurn() {
        while (compStep()) {
            if (checkWinner()) return;
        }
        if (checkWinner()) gameScreen.askPlayerStep();
    }

    private boolean compStep() {
        Field player1Field = game.getPlayer1Field();
        if (shotTheSameShipCount == 0) {
            return firstCompShot(player1Field);
        } else if (shotTheSameShipCount == 1) { //попали один раз и не знаем в каком направл находится корабль
            /*
            1. получить следующую ячейку в заданном направлении
            2. если в нее стреляли - перейти к следующему направлению (вернуться к п.1)
            3. выстрелить в эту ячейку
            4. проверить попали или нет
            4.1 если не попали - перейти к следующему направлению
            4.2 если попали - увеличить shotTheSameShipCount
             */
            return secondCompShot(player1Field);
        } else { //знаем ШипДирекшион
            /*
            1. Получить следующую ячейку в заданном направлении
            2. Если в нее не стреляли, то выстрелить
            2.1 Если не попали - сменить направление на противоположное
            2.2 Если попали - проверить потопили или нет (если потопили - ShotTheSameShipCount=0)
             */
            return otherCompShot(player1Field);
        }
    }

    private boolean firstCompShot(Field player1Field) {
        Point point = Point.createRandom();
        player1Field.shootAt(point);
        Cell cell = player1Field.getCell(point.row, point.column);
        Ship ship = player1Field.findShip(point);
        shotTheSameShipCount = (ship != null && !ship.isSunk()) ? 1 : 0;
        firstPoint = lastPoint = point;
        direction = ShootDirection.Right;
        gameScreen.notifyCompShot(point, cell.isShip(), ship != null && ship.isSunk());
        return cell.isShip();
    }

    private boolean secondCompShot(Field player1Field) {
        for (; ; ) {
            int dirValue = direction.ordinal();
            Point nextPoint = direction.getNextPoint(lastPoint);
            if (!nextPoint.isValid()) {
                changeDirection(dirValue, 1);
                continue;
            }
            Cell cell = player1Field.getCell(nextPoint.row, nextPoint.column);
            if (!cell.isShot()) {
                makeCompShot(player1Field, dirValue, nextPoint, cell, 1);
                return cell.isShip();
            } else {
                changeDirection(dirValue, 1);
            }
        }
    }

    private boolean otherCompShot(Field player1Field) {
        while (true) {
            int dirValue = direction.ordinal();
            Point nextPoint = direction.getNextPoint(lastPoint);
            if (!nextPoint.isValid()) {
                changeDirection(dirValue, 2);
                continue;
            }
            Cell cell = player1Field.getCell(nextPoint.row, nextPoint.column);
            if (!cell.isShot()) {
                makeCompShot(player1Field, dirValue, nextPoint, cell, 2);
                return cell.isShip();
            } else {
                changeDirection(dirValue, 2);
            }
        }
    }

    private void makeCompShot(Field player1Field, int dirValue, Point nextPoint, Cell cell, int dirChangeStep) {
        player1Field.shootAt(nextPoint);
        lastPoint = nextPoint;
        Ship ship = player1Field.findShip(lastPoint);
        gameScreen.notifyCompShot(nextPoint, cell.isShip(), ship != null && ship.isSunk());
        if (!cell.isShip()) {
            changeDirection(dirValue, dirChangeStep);
        } else {
            if (ship.isSunk()) {
                shotTheSameShipCount = 0;
            } else {
                shotTheSameShipCount++;
            }
        }
    }

    private void changeDirection(int dirValue, int dirChangeStep) {
        direction = ShootDirection.values()[(dirValue + dirChangeStep) % 4];
        lastPoint = firstPoint;
    }

//    private boolean compStep() {
//        Point point = Point.createRandom();
//        Field player1Field = game.getPlayer1Field();
//        player1Field.shootAt(point);
//        Cell cell = player1Field.getCell(point.row, point.column);
//        return cell.isShip();
//    }

    @Override
    public void run() {
        //gameScreen.show();
        gameScreen.askPlayerStep();
    }

    private boolean checkWinner() {
        boolean playerIsWinner = game.getPlayer2Field().areAllSunk();
        boolean compIsWinner = game.getPlayer1Field().areAllSunk();
        if (playerIsWinner) {
            game.setState(new GameResultState(game, true, factory));
            return true;
        }
        if (compIsWinner) {
            game.setState(new GameResultState(game, false, factory));
            return true;
        }
        return false;
    }
}
