package org.example.controllers;

import org.example.InvalidShipException;
import org.example.models.Field;
import org.example.models.Point;
import org.example.models.Ship;
import org.example.models.ShipDirection;
import org.example.views.IShipPlacementScreen;
import org.example.views.IViewFactory;

import java.util.Random;

public class ShipPlacementState implements GameState {
    private final Game game;
    private IShipPlacementScreen screen;
    private Random random = new Random();
    private IViewFactory factory;

    public ShipPlacementState(Game game, IViewFactory factory) {
        this.game = game;
        screen = factory.createShipPlacementScreen(game.getPlayer1Field());
        this.factory = factory;
        //screen.setRandomPlacementHandler(this::handleRandomPlacement);
        /**
         * ивент расстановки кораблей для выбора автоматической расстановки
         * в Гуи-приложении этот ивент можно вызывать несколько раз если пользователя результат не устроил
         */
        screen.setRandomPlacementHandler(() -> {
            generateRandomShips(game.getPlayer1Field());
        });
    }

    @Override
    public void run() {
        screen.display();
        generateRandomShips(game.getPlayer2Field());
        game.setState(new BattleState(game, factory));
    }

    private void handleRandomPlacement() {
        generateRandomShips(game.getPlayer1Field());
    }

    private void generateRandomShips(Field field) {
        int[] deckSizes = {4, 3, 3, 2, 2, 2, 1, 1, 1, 1};
        for (int deckSize : deckSizes) {
            while (true) {
                Ship ship = generateShipPlacement(deckSize);
                try {
                    field.addShip(ship);
                    break;
                } catch (InvalidShipException e) {
                }
            }
        }
    }

    private Ship generateShipPlacement(int deckSize) {
        ShipDirection shipDirection = ShipDirection.createRandom();
        Point point = Point.createRandom();
        return new Ship(point, shipDirection, deckSize);
    }
}

//todo: homeTask13.07.21
/*
преобразовать гейм-контроллер в бэттлСтэйт
атомики по многопоточности
 */

//todo: homeTask 10.08.21
/**
 * 1. протестировать переход в состояние результата (как победа, так и поражение)
 * 2. реализовать все туду
 */