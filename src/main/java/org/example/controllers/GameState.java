package org.example.controllers;

public interface GameState {
    void run();
}
