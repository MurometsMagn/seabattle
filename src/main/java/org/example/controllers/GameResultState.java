package org.example.controllers;

import org.example.views.IScreen;
import org.example.views.IViewFactory;

public class GameResultState implements GameState{
    private final Game game;
    private final IScreen screen;

    public GameResultState(Game game, boolean isWinner, IViewFactory factory) {
        this.game = game;
        screen = factory.createGameResultScreen(isWinner);
    }

    @Override
    public void run() {
        screen.display();
        game.stop();
    }
}
