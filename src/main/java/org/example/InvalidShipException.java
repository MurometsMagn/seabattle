package org.example;

public class InvalidShipException extends Exception {

    public InvalidShipException(String message) {
        super(message);
    }
}
