package org.example;

//import org.example.views.console.ConsolePlay;

import org.example.controllers.Game;

public class Main {
    public static void main(String[] args) {
        //System.out.println('8' - '6');
       //System.out.println('b' - 'a');
        //System.out.println(Character.getNumericValue('8'));
//        System.out.println((int) '8');
//        System.out.println((int) '0');
//        System.out.println((int) '7');
 //       System.out.println((int) 'a');

        try {
            //new Game(new org.example.views.console.ViewFactory()).run();
            new Game(new org.example.views.swing.ViewFactory()).run();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
