package org.example.models;

public class Ship {
    //    private int row;
//    private int column;
    private final ShipDirection shipDirection;
    private final int size;
    private int hits = 0; //попаданий
    private final Rectangle rectangle = new Rectangle();

    public Ship(Point point, ShipDirection shipDirection, int size) {
        rectangle.topLeft = point;
        if (shipDirection == ShipDirection.Vertical) {
            rectangle.width = 1;
            rectangle.height = size;
        } else {
            rectangle.width = size;
            rectangle.height = 1;
        }
        this.shipDirection = shipDirection;
        this.size = size;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    //проверка на пересекаемость с соседним кораблем
    public boolean canAddShip(Ship comparableShip) {
//        int rowMin = comparableShip.row - 1;
//        if (rowMin < 1) rowMin =1 ;
//        int rowMax = comparableShip.row + size;
//        if (rowMax > 10) rowMax = 10;
//        int colMin = comparableShip.column - 1;
//        if (colMin < 1) colMin = 1;
//        int colMax = comparableShip.column + size;
//        if (colMax > 10) colMax = 10;
//        for (int i = rowMin; i <= rowMax; i++) {
//            for (int j = colMin; j <= colMax; j++) {
//                if (i == row && j == column) return false;
//                //вызвать containsCellImpl
//            }
//        }
        //return !this.rectangle.expandExtend().isOverlap(comparableShip.rectangle.expandExtend());
        return !this.rectangle.isOverlap(comparableShip.rectangle);
    }

    //проверка попадания точки в прямоугольник
    //запрашиваемая ячейка относится к кораблю
    public boolean containsPoint(Point point) {
        return rectangle.containsPoint(point);
    }

    //сбит
    public boolean isSunk() {
        return (hits == size);
    }

    public void addHit() {
        hits++;
    }

    public int getSize() {
        return size;
    }

    public Point getDeck(int deckNumber) {
        int row = rectangle.topLeft.row;
        int col = rectangle.topLeft.column;
        if (shipDirection == ShipDirection.Horizontal) {
            return new Point(row, col + deckNumber - 1);
        }
        return new Point(row + deckNumber - 1, col);
    }
}

/* 15.06.21
  containsCellImpl
 */