package org.example.models;

import java.util.Random;

public enum ShipDirection {
    Horizontal,
    Vertical;

    private static final Random random = new Random();

    public static ShipDirection createRandom() {
        return random.nextBoolean() ? Horizontal : Vertical;
    }
}


