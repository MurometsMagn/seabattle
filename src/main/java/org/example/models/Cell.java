package org.example.models;

public class Cell {
    private final Point point = new Point();
    //private String content = " ";
    private boolean shot = false;
    private boolean ship = false;

    //нужен ли конструктор (два конструктора)?
    public Cell(int row, int column) {
        point.row = row;
        point.column = column;
    }


    public int getRow() {
        return point.row;
    }

    public int getColumn() {
        return point.column;
    }

//    public String getContent() {
//        return content;
//    }

//    public void setContent(String content) {
//        this.content = content;
//    }

    public boolean isShot() {
        return shot;
    }

    public void hit() {
        shot = true;
        //content = ship ? "X" : ".";
    }

    public boolean isShip() {
        return ship;
    }

    public void setShip(boolean ship) {
        this.ship = ship;
        //content = "O";
    }
}
