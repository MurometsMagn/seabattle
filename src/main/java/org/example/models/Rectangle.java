package org.example.models;

public class Rectangle {
    //левый верхний угол
    public Point topLeft;
    public int width;
    public int height;

    public Rectangle(Point topLeft, int width, int height) {
        this.topLeft = topLeft;
        this.width = width;
        this.height = height;
    }

    public Rectangle() {
        topLeft = new Point(1, 1);
        width = 1;
        height = 1;
    }

    /* проверка пересечения двух прямоугольников
https://gamedev.stackexchange.com/questions/586/what-is-the-fastest-way-to-work-out-2d-bounding-box-intersection
    bool DoBoxesIntersect(Box a, Box b) {
  return (abs((a.x + a.width/2) - (b.x + b.width/2)) * 2 < (a.width + b.width)) &&
         (abs((a.y + a.height/2) - (b.y + b.height/2)) * 2 < (a.height + b.height));
}
  this = a
  other = b
  x = column
  y = row
     */
//    public boolean isOverlap(Rectangle other) {
//        return (Math.abs((topLeft.column + width/2) - (other.topLeft.column + other.width/2)) * 2 < (width + other.width)) &&
//                (Math.abs((topLeft.row + height/2) - (other.topLeft.row + other.height/2)) * 2 < (height + other.height));
//
//    }

//    public boolean isOverlap(Rectangle other) {
//        return !(other.topLeft.column > (topLeft.column + width)
//                || (other.topLeft.column + other.width) < topLeft.column
//                || other.topLeft.row > (topLeft.row + height)
//                || (other.topLeft.row + other.height) < topLeft.row);
//
//    }

    public boolean isOverlap(Rectangle other) { //все тесты проходят
        return (topLeft.column <= other.topLeft.column + other.width &&
                topLeft.column + width >= other.topLeft.column &&
                topLeft.row <= other.topLeft.row + other.height &&
                topLeft.row + height >= other.topLeft.row);
    }

    public boolean containsPoint(Point other) {
        return (this.topLeft.column <= other.column &&
                this.topLeft.row <= other.row &&
                (this.topLeft.column + width) > other.column &&
                (this.topLeft.row + height) > other.row);
    }

    public Rectangle expandExtend() {
        Rectangle extended = new Rectangle();
        extended.topLeft.row = this.topLeft.row - 1;
        extended.topLeft.column = this.topLeft.column - 1;
        extended.width = this.width + 2;
        extended.height = this.height + 2;

        return extended;
    }
}
