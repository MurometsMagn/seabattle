package org.example.models;

import org.example.InvalidShipException;

import java.util.ArrayList;
import java.util.List;

public class Field {
    private String name;
    private List<List<Cell>> cells;
    private List<Ship> ships = new ArrayList<>();

    public Field() {
        cells = new ArrayList<>(10);
        for (int i = 1; i <= 10; i++) {
            List<Cell> row = new ArrayList<>(10);
            for (int j = 1; j <= 10; j++) {
                Cell cell = new Cell(i, j);
                row.add(cell);
            }
            cells.add(row);
        }
    }

    public void addShip(Ship newShip) throws InvalidShipException {
        Rectangle shipRectangle = newShip.getRectangle();
        Point topLeft = shipRectangle.topLeft;
        if (topLeft.column < 1 || topLeft.row < 1 ||
                topLeft.column + (shipRectangle.width - 1) > 10 || topLeft.row + (shipRectangle.height - 1) > 10) {
            throw new InvalidShipException("невозможно добавить корабль");
        }
        for (Ship ship : ships) {
            if (!ship.canAddShip(newShip)) {
                throw new InvalidShipException("невозможно добавить корабль");
            }
        }
        ships.add(newShip);

        for (int i = 1; i <= newShip.getSize(); i++) {
            Point deck = newShip.getDeck(i);
            Cell cell = getCellByPoint(deck);
            cell.setShip(true);
        }
    }

    public void shootAt(Point point) {
        Cell cell = getCellByPoint(point);
        if (cell.isShot()) return;
        cell.hit();
        if (cell.isShip()) {
            Ship ship = findShip(point);
            ship.addHit();
        }
    }

    public Ship findShip(Point point) {
        for (Ship ship : ships) {
            if (ship.containsPoint(point)) {
                return ship;
            }
        }

        return null;
        //return new Ship(); //что возвращать если клетка не попала в корабль?
    }

    private Cell getCellByPoint(Point point) {
        return cells.get(point.row - 1).get(point.column - 1);
    }

    public Cell getCell(int row, int col) {
        return cells.get(row - 1).get(col - 1);
    }

    public boolean areAllSunk() {
        for (Ship ship : ships) {
            if (!ship.isSunk()) return false;
        }
        return true;
    }

    public void clear() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                cells.get(i).set(j, new Cell(i + 1, j + 1));
            }
        }
        ships.clear();
    }
}
