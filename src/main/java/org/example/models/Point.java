package org.example.models;

import org.example.InvalidCoordinateException;

import java.util.Locale;
import java.util.Random;

public class Point {
    public int row;
    public int column;
    private static final Random random = new Random();

    public Point(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public Point() {
        row = 1;
        column = 1;
    }

    @Override
    public String toString() {
        char letter = (char) ('A' + (column - 1));

        return "" + letter + row;
    }

    public static Point fromString(String coord) throws InvalidCoordinateException {
        if (coord == null || coord.isEmpty()) {
            throw new InvalidCoordinateException("Coordinate must not be empty");
        }
        coord = coord.toLowerCase(Locale.ROOT).trim();
        int column = getColumn(coord);
        int row = getRow(coord);
        return new Point(row, column);
    }

    private static int getColumn(String coord) throws InvalidCoordinateException {
        int column = coord.charAt(0) - 'a' + 1;
        if (column < 1 || column > 10) {
            throw new InvalidCoordinateException("input column is incorrect");
        }
        return column;
    }

    private static int getRow(String coord) throws InvalidCoordinateException {
        String digits = coord.substring(1);
        int row;
        try {
            row = Integer.parseInt(digits);
        } catch (NumberFormatException numberFormatException) {
            throw new InvalidCoordinateException("row must contain only digits");
        }
        if (row < 1 || row > 10) {
            throw new InvalidCoordinateException("input row is incorrect");
        }
        return row;
    }

    public static Point createRandom() {
        return new Point(random.nextInt(10 - 1 + 1) + 1, random.nextInt(10 - 1 + 1) + 1);
    }

    //проверка на попадание точки в поле
    public boolean isValid() {
        return (row >= 1 && row <= 10 && column >= 1 && column <= 10);
    }
}
