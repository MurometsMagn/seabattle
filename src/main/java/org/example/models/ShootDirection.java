package org.example.models;

public enum ShootDirection {
    Right {
        @Override
        public Point getNextPoint(Point point) {
            return new Point(point.row, point.column + 1);
        }
    },
    Down {
        @Override
        public Point getNextPoint(Point point) {
            return new Point(point.row + 1, point.column);
        }
    },
    Left {
        @Override
        public Point getNextPoint(Point point) {
            return new Point(point.row, point.column - 1);
        }
    },
    Up {
        @Override
        public Point getNextPoint(Point point) {
            return new Point(point.row - 1, point.column);
        }
    };

    public abstract Point getNextPoint(Point point);
}
