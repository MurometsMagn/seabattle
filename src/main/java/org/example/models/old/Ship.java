package org.example.models.old;

public class Ship {
    private final boolean[][] shipShape;
    private final int x = 0;
    private final int y = 0;

    public Ship(int shipLength) {
        this.shipShape = new boolean[shipLength][shipLength];
    }
}
