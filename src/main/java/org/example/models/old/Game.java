package org.example.models.old;

import org.example.MyOwnLib;

import java.util.Random;

public class Game {
    public int[][] firstPlayerField;
    public int[][] secondPlayerField;
    Random random = new Random();

    public int[][] generateShip(int shipLength, int[][] field) {
        int[][] shipShape = zeroInicialise(10,10);

        //инициализация положения первой детали
        int x = 0;
        int y = 0;
        do {
            x = MyOwnLib.randomInt2(0, 9);
            y = MyOwnLib.randomInt2(0, 9);
        } while (!neighbourShipExist(x, y, field));
        if (shipLength == 1) {
            shipShape[x][y] = 1;
            return shipShape;
        }
        shipShape[x][y] = 8; //временное значение

        for (int i = 2; i <= shipLength; i++) {
            //инициализация положения второй детали (х +-1 или у +- 1)
            //проверка на невыход из поля
            //проверка на попадание в сам корабль
            //проверка на наличие соседнего корабля
        }

        return shipShape;
    }

    //проверка на наличие соседнего корабля
    private boolean neighbourShipExist(int x, int y, int[][] field) {


        return false;
    }

//    private int[][] initFirstUnit() {
//        int x = MyOwnLib.randomInt2(0, 9);
//        int y = MyOwnLib.randomInt2(0, 9);
//    }

    public static int[][] zeroInicialise(int x, int y) {
        int[][] zeroSpace = new int[x][y];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                zeroSpace[i][j] = 1;
            }
        }
        return zeroSpace;
    }

    public static int[][] zeroInicialise() {
        return zeroInicialise(10, 10);
    }
}
