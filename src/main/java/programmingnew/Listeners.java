package programmingnew;

import java.awt.event.*;

public class Listeners implements MouseListener, KeyListener, MouseMotionListener {
    @Override
    public void keyTyped(KeyEvent e) {
        char sim = e.getKeyChar();//получаем символ клавиши
        if (Panel.c_menue.button_up.zamen) {//присвоим надписи символ как строка
            Panel.c_menue.button_up.f = String.valueOf(sim);
            Panel.c_menue.button_up.zamen = false;
        }
        if (Panel.c_menue.button_d.zamen) {//присвоим надписи символ как строка
            Panel.c_menue.button_d.f = String.valueOf(sim);
            Panel.c_menue.button_d.zamen = false;
        }
        if (Panel.c_menue.button_l.zamen) {//присвоим надписи символ как строка
            Panel.c_menue.button_l.f = String.valueOf(sim);
            Panel.c_menue.button_l.zamen = false;
        }
        if (Panel.c_menue.button_r.zamen) {//присвоим надписи символ как строка
            Panel.c_menue.button_r.f = String.valueOf(sim);
            Panel.c_menue.button_r.zamen = false;
        }
        if (Panel.c_menue.button_f.zamen) {//присвоим надписи символ как строка
            Panel.c_menue.button_f.f = String.valueOf(sim);
            Panel.c_menue.button_f.zamen = false;
        }
    }

    //проверка нажатой клавиши
    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();//получить код нажатой клавиши
        //проверка клавиши
        if (key == Panel.c_menue.button_up.ch_code) {
            Player.up = true;
        }
        if (key == Panel.c_menue.button_d.ch_code) {
            Player.down = true;
        }
        if (key == Panel.c_menue.button_l.ch_code) {
            Player.left = true;
        }
        if (key == Panel.c_menue.button_r.ch_code) {
            Player.right = true;
        }
        if (key == KeyEvent.VK_ESCAPE) {
            if (Panel.state == Panel.STATES.PLAY) Panel.state = Panel.STATES.MENUE;//переход в меню из игры
        }
        if (Panel.c_menue.button_up.zamen) {
            Panel.c_menue.button_up.ch_code = e.getKeyCode();
        }
        if (Panel.c_menue.button_d.zamen) {
            Panel.c_menue.button_d.ch_code = e.getKeyCode();
        }
        if (Panel.c_menue.button_l.zamen) {
            Panel.c_menue.button_l.ch_code = e.getKeyCode();
        }
        if (Panel.c_menue.button_r.zamen) {
            Panel.c_menue.button_r.ch_code = e.getKeyCode();
        }
    }

    //проверка отжатой клавиши
    @Override
    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();

        if (key == Panel.c_menue.button_up.ch_code) {
            Player.up = false;
        }
        if (key == Panel.c_menue.button_d.ch_code) {
            Player.down = false;
        }
        if (key == Panel.c_menue.button_l.ch_code) {
            Player.left = false;
        }
        if (key == Panel.c_menue.button_r.ch_code) {
            Player.right = false;
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            if (Panel.state == Panel.STATES.MENUE) {
                Menue.click = true;//нажатие ЛКМ в меню
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            if (Panel.state == Panel.STATES.MENUE) {
                Menue.click = false;//отпуск ЛКМ в меню
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {//метод переноса мышкой

    }

    @Override
    public void mouseMoved(MouseEvent e) {
        Panel.mouseX = e.getX();//получить координату х при перемещении мышки
        Panel.mouseY = e.getY();//получить координату y при перемещении мышки

    }
}
