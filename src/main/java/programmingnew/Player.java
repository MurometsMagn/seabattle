package programmingnew;


import javax.swing.*;
import java.awt.*;

public class Player {
    //нач.координаты и размер объекта
    private double x;//координата х героя
    private double y;//координата y героя
    private double w;//ширина объекта
    private double h;//высота объекта

    private int speed;//скорость

    //клавиши перемещения
    public static boolean up;
    public static boolean down;
    public static boolean left;
    public static boolean right;

    Image img = new ImageIcon("image/player.png").getImage();

    public Player() {//конструктор
        //нач.координаты героя
        x = 400;
        y = 500;
        //размеры картинки героя
        w = 188;
        h = 309;
        speed = 5;//скорость перемещения героя
        //нач.значения клавиш
        up = false;
        down = false;
        left = false;
        right = false;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getW() {
        return w;
    }

    public double getH() {
        return h;
    }

    //обновления
    //законы движения героя по нажатию клавиш клавиатуры
    public void update() {
        if (Panel.easy) speed = 10;
        if (Panel.norm) speed = 4;
        if (Panel.hard) speed = 2;

        //смещение героя по игровому полю
        if (up && y > 20) {
            y -= speed;
        }
        if (down && y < (Panel.HEIGHT - h)) {
            y += speed;
        }
        if (left && x > 0) {
            x -= speed;
        }
        if (right && x < (Panel.WIDTH - w)) {
            x += speed;
        }
    }

    //отрисовка героя
    public void draw(Graphics2D g) {
        g.drawImage(img, (int) x, (int) y, null);
    }
}
