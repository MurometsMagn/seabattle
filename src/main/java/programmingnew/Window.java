package programmingnew;

import javax.swing.*;
import java.awt.*;

/**
 * Конструктор для игр
 */
public class Window {
    public static void main(String[] args) {

        JFrame startFrame = new JFrame("game");//создаем окно с названием
        startFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//закрытие окна при клике крестика
        startFrame.setLocation(0, 0);//изменяем местоположение фрейма
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();//получим размер окна
        startFrame.setSize(screenSize);//установим его
        //startFrame.setSize(800, 450);//размер окна

        startFrame.add(new Panel());
        //startFrame.setContentPane(new Panel());

        //окно видимо
        startFrame.setVisible(true);
    }
}
