package programmingnew;


import javax.swing.*;
import java.awt.*;

public class Menue {
    public static boolean click = false;//клик мышкой в режиме меню

    ButtMenue button1 = new ButtMenue(10, 10, 396, 100, "image/btn1.png", "Новый игрок");
    ButtMenue button2 = new ButtMenue(10, 150, 396, 100, "image/btn1.png", "Играть");
    ButtMenue button3 = new ButtMenue(10, 300, 396, 100, "image/btn1.png", "Настройки");
    ButtMenue button4 = new ButtMenue(10, 450, 396, 100, "image/btn1.png", "Правила");
    ButtMenue button5 = new ButtMenue(10, 600, 396, 100, "image/btn1.png", "Выход");

    public void draw(Graphics2D g) {
        button1.draw(g);
        button2.draw(g);
        button3.draw(g);
        button4.draw(g);
        button5.draw(g);
    }

   class ButtMenue {
       //нач.координаты и размер объекта
       private double x;//координата х
       private double y;//координата y
       private double w;//ширина объекта
       private double h;//высота объекта

       private Color color1;//цвет шрифта
       public String f;//надпись на кнопкe
       public String s;//путь к картинке кнопки

       //Constructor
       public ButtMenue(int x, int y, int w, int h, String s, String f) {
           this.x = x;
           this.y = y;
           this.w = w;
           this.h = h;
           color1 = Color.WHITE;
           this.f = f;
           this.s = s;
       }

       //геттеры
       public double getX() {
           return this.x;
       }

       public double getY() {
           return this.y;
       }

       public double getW() {
           return this.w;
       }

       public double getH() {
           return this.h;
       }

       //отрисовка объекта
       public void draw(Graphics2D g) {
           g.drawImage(new ImageIcon(s).getImage(), (int) x, (int) y, null);//отрисовываем элемент в координатах
           g.setColor(color1);//задаем цвет объекту Color
           Font font = new Font("Arial", Font.ITALIC, 60);//создаем объект класса фонт (передаем в конструктор параметры)
           g.setFont(font);//устанавливаем наш шрифт
           long length = (int) g.getFontMetrics().getStringBounds(f, g).getWidth();//длина надписи в пикселях
           g.drawString(f, (int) (x + w / 2) - (int) (length / 2), (int) y + (int)(h / 3) * 2);//рисуем строчку в центре панели
       }

       //попадание курсора на кнопку меню
       public void moveButt() {
           if (Panel.mouseX > getX() && Panel.mouseX < getX() + getW() &&
                   Panel.mouseY > getY() &&
                   Panel.mouseY < (getY()  + getH())) {//если курсор попал на кнопку
               s = "image/btn2.png";
               if (equals(button1)) {
                   f = "new user";}
               if (equals(button2)) {
                   f = "game";
                   if (click) {//клик ЛКМ
                       Panel.state = Panel.STATES.PLAY;//переход в игру
                       click = false;
                   }
               }
               if (equals(button3)) {
                   f = "settings";
                   if (Menue.click) {
                       Panel.settmenue = true;
                       Panel.buttmenue = false;
                   }
               }
               if (equals(button4)) {
                   f = "specification";}
               if (equals(button5)) {
                   f = "exit";
                   if (click) {
                       System.exit(0);
                   }
               }
           } else {
               s = "image/btn1.png";
               if (equals(button1)) {
                   f = "новый игрок";}
               if (equals(button2)) {
                   f = "играть";}
               if (equals(button3)) {
                   f = "настройки";}
               if (equals(button4)) {
                   f = "правила";}
               if (equals(button5)) {
                   f = "выход";}
           }
       }
   }
}
