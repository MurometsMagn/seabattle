package programmingnew;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Панель игры
 */
public class Panel extends JPanel implements ActionListener {

    //размер панели
    public static int WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width;
    public static int HEIGHT = Toolkit.getDefaultToolkit().getScreenSize().height;
    //координаты мышки
    public static int mouseX;
    public static int mouseY;

    //активные страницы меню
    public static boolean buttmenue = true; //основное меню
    public static boolean settmenue = false;//окно меню настройки
    public static boolean controlmenue = false;//страница меню настройки

    //уровень сложности
    public static boolean easy = true;
    public static boolean norm = false;
    public static boolean hard = false;
    public static boolean aud = true;
    public static boolean control = true;//управление по умолчанию

    public static ControlMenue c_menue;

    public static enum STATES {MENUE, PLAY}

    //объявляем перечисления
    public static STATES state = STATES.MENUE;//переменная игры по изначально меню

    private BufferedImage image; //ссылка на объект класса
    private Graphics2D g;//ссылка на объект класса

    //список кнопок
    public ArrayList<SettMenue> buttons;

    Timer mainTimer = new Timer(30, this); //Таймер

    Back back = new Back();
    Player player = new Player();
    Menue menue = new Menue();

    public Panel() {//конструктор
        super();//активируем конструктор родителя
        setFocusable(true);//передаем фокус
        requestFocus();//активируем
        mainTimer.start();//запуск Таймера

        c_menue = new ControlMenue();

        image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);//создаем объект буфера для хранения картинок
        g = (Graphics2D) image.getGraphics();//граф.объекту присвоим элемент буфера - картинка Graphics2D применив метод getGraphics()

        addMouseListener(new Listeners());//добавляем обработчик событий клик мышь
        addKeyListener(new Listeners());//добавляем обработчик событий клава
        addMouseMotionListener(new Listeners());//добавляем обработчик событий перемещение мышь

        buttons = new ArrayList<SettMenue>();
        buttons.add(new SettMenue(500, 65, 100, 37, "", "легко"));
        buttons.add(new SettMenue(650, 65, 100, 37, "", "средне"));
        buttons.add(new SettMenue(800, 65, 100, 37, "", "сложно"));

        buttons.add(new SettMenue(500, 210, 100, 37, "", "вкл"));
        buttons.add(new SettMenue(650, 210, 100, 37, "", "выкл"));

        buttons.add(new SettMenue(500, 365, 100, 37, "", "стандарт"));
        buttons.add(new SettMenue(650, 365, 100, 37, "", "пользоват"));

        buttons.add(new SettMenue(1250, 20, 100, 37, "", "назад"));
    }


    @Override
    public void actionPerformed(ActionEvent e) {//все события игры
        if (state.equals(STATES.MENUE)) {//меню
            back.draw(g);//отобразить фон

            if (buttmenue) {
                menue.draw(g);//отобразить меню
                menue.button1.moveButt();
                menue.button2.moveButt();
                menue.button3.moveButt();
                menue.button4.moveButt();
                menue.button5.moveButt();
            }
            if (settmenue) {//страница меню настроек
                moveSettButt();
            }
            if (controlmenue) {
                c_menue.draw(g);
                c_menue.moveContr(c_menue.button_up);
                c_menue.moveContr(c_menue.button_d);
                c_menue.moveContr(c_menue.button_l);
                c_menue.moveContr(c_menue.button_r);
                c_menue.moveContr(c_menue.button_f);
                c_menue.moveContr(c_menue.button_k);
            }

            gameDraw();//прорисовать в панели}
        }
        if (state.equals(STATES.PLAY)) {//игра
            gameUpdate();
            gameRander();
            gameDraw();
        }
    }

    public void moveSettButt() {
        for (int i = 0; i < buttons.size(); i++) {
            buttons.get(i).draw(g);
            if (Panel.mouseX > buttons.get(i).getX() && Panel.mouseX < buttons.get(i).getX() + buttons.get(i).getW() &&
                    Panel.mouseY > buttons.get(i).getY() &&
                    Panel.mouseY < (buttons.get(i).getY() + buttons.get(i).getH())) {
                buttons.get(i).s = "image/btn3.png";
                if (i == 0) {
                    if (Menue.click) {
                        easy = true;
                        norm = false;
                        hard = false;
                    }
                } else if (i == 1) {
                    if (Menue.click) {
                        easy = false;
                        norm = true;
                        hard = false;
                    }
                } else if (i == 2) {
                    if (Menue.click) {
                        easy = false;
                        norm = false;
                        hard = true;
                    }
                }
                if (i == 3) {
                    if (Menue.click) {
                        aud = true;
                    }
                } else if (i == 4) {
                    if (Menue.click) {
                        aud = false;
                    }
                }
                if (i == 5) {//клавиша стандарт
                    if (Menue.click) {
                        control = true;
                        c_menue.button_up.f = "курс вверх";
                        c_menue.button_d.f = "курс вниз";
                        c_menue.button_l.f = "курс влево";
                        c_menue.button_r.f = "курс вправо";
                        c_menue.button_f.f = "Space";
                        //передача кодов клавиш стандартного управления
                        c_menue.button_up.ch_code = 38;
                        c_menue.button_d.ch_code = 40;
                        c_menue.button_l.ch_code = 37;
                        c_menue.button_r.ch_code = 39;
                        c_menue.button_f.ch_code = 32;
                    }
                } else if (i == 6) {
                    if (Menue.click) {
                        control = false;
                        settmenue = false;
                        controlmenue = true;
                    }
                }
                if (i == 7) {
                    if (Menue.click) {
                        settmenue = false;
                        buttmenue = true;
                    }
                }
            } else {
                buttons.get(i).s = "image/btn4.png";
            }
        }
    }

    public void gameRander() {// рисование виртуальных объектов в отдельном окне
        back.draw(g);
        player.draw(g);
    }

    public void gameUpdate() {//обновление
        player.update();
    }

    private void gameDraw() {
        Graphics g2 = this.getGraphics();//передопред Graphics2d на Graphics
        g2.drawImage(image, 0, 0, null);//рисуем
        g2.dispose();//команда для уборки мусора
    }
}
