package programmingnew;

import javax.swing.*;
import java.awt.*;

/**
 * Окно меню для изменения кнопок управления
 * (Нужно было наследовать от класса Menue)
 */
public class ControlMenue {
    //объявления кнопок
    ButtControl button_up = new ButtControl(550, 10, 100, 37, "image/btn4.png", "курс вверх", 38);
    ButtControl button_d = new ButtControl(550, 110, 100, 37, "image/btn4.png", "курс вниз",40);
    ButtControl button_l = new ButtControl(550, 210, 100, 37, "image/btn4.png", "курс влево",37);
    ButtControl button_r = new ButtControl(550, 310, 100, 37, "image/btn4.png", "курс вправо",39);
    ButtControl button_f = new ButtControl(550, 410, 100, 37, "image/btn4.png", "Space",32);
    ButtControl button_k = new ButtControl(50, 20, 100, 37, "image/btn4.png", "назад",8);



    //вызов метода для прорисовки кнопки
    public void draw(Graphics2D g) {
        button_up.draw(g);
        button_d.draw(g);
        button_l.draw(g);
        button_r.draw(g);
        button_f.draw(g);
        button_k.draw(g);

        g.drawImage(new ImageIcon("image/keys1.png").getImage(),100,200,null);
    }

    public void moveContr(ButtControl b) {
        if (Panel.mouseX > b.getX() && Panel.mouseX < b.getX() + b.getW() &&
                Panel.mouseY > b.getY() &&
                Panel.mouseY < (b.getY() + b.getH())) {//если курсор попал на кнопку
            b.s = "image/btn3.png";
            if (b.equals(button_up)) {
                editContr(b);//метод активации кнопки для замены клавиши управления
            } else if (b.equals(button_d)) {
                editContr(b);
            } else if (b.equals(button_l)) {
                editContr(b);//метод активации кнопки для замены клавиши управления
            } else if (b.equals(button_r)) {
                editContr(b);//метод активации кнопки для замены клавиши управления
            } else if (b.equals(button_f)) {
                editContr(b);//метод активации кнопки для замены клавиши управления
            } else if (b.equals(button_k)) {
                if (Menue.click) {
                    Panel.buttmenue = false;//основное окно - не активно
                    Panel.controlmenue = false;//меню кнопок контроля - не активно
                    Panel.settmenue = true;//меню настройки - активно
                }
            }
        } else {
            b.s = "image/btn4.png";//адрес картинки не активной кнопки
            b.zamen = false;//замена запрещена
        }
    }

    //попадание курсора на кнопку меню
    public void editContr(ButtControl b) {
        if (Menue.click) {
            b.f = "";//надпись на кнопке стерта
            b.zamen = true;//замена разрешена
        }
    }


    class ButtControl {
        //нач.координаты и размер объекта
        private double x;//координата х
        private double y;//координата y
        private double w;//ширина объекта
        private double h;//высота объекта

        private Color color1;//цвет шрифта
        public String f;//надпись на кнопкe
        public int ch_code;//код символа кнопки
        public String s;//путь к картинке кнопки

        public boolean zamen = false;//разрешение на замену клавиши

        //Constructor
        public ButtControl(int x, int y, int w, int h, String s, String f, int ch_code) {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            color1 = Color.BLACK;
            this.f = f;
            this.s = s;
            this.zamen = zamen;
            this.ch_code = ch_code;
        }

        //геттеры
        public double getX() {
            return this.x;
        }

        public double getY() {
            return this.y;
        }

        public double getW() {
            return this.w;
        }

        public double getH() {
            return this.h;
        }


        public void draw(Graphics2D g) {
            g.drawImage(new ImageIcon(s).getImage(), (int) x, (int) y, null);//отрисовываем элемент в координатах
            g.setColor(color1);//задаем цвет объекту Color
            Font font1 = new Font("Arial", Font.ITALIC, 18);//создаем объект класса фонт (передаем в конструктор параметры)
            g.setFont(font1);//устанавливаем наш шрифт
            long length = (int) g.getFontMetrics().getStringBounds(f, g).getWidth();//длина надписи в пикселях
            g.drawString(f, (int) (x + w / 2) - (int) (length / 2), (int) y + (int) (h / 3) * 2);//рисуем строчку в центре панели

            g.setColor(Color.red);//задаем цвет объекту Color
            Font font2 = new Font("Arial", Font.ITALIC, 40);//создаем объект класса фонт (передаем в конструктор параметры)
            g.setFont(font2);//устанавливаем наш шрифт
            g.drawString("движение вверх", 750, 40);
            g.drawString("движение вниз", 750, 140);
            g.drawString("движение влево", 750, 240);
            g.drawString("движение вправо", 750, 340);
            g.drawString("        удар", 750, 440);
        }
    }
}
