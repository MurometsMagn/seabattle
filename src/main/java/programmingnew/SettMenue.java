package programmingnew;

import javax.swing.*;
import java.awt.*;

public class SettMenue {
    //нач.координаты и размер объекта
    private double x;//координата х
    private double y;//координата y
    private double w;//ширина объекта
    private double h;//высота объекта

    private Color color1;//цвет шрифта
    public String f;//надпись на кнопкe
    public String s;//путь к картинке кнопки

    //Constructor
    public SettMenue(int x, int y, int w, int h, String s, String f) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        color1 = Color.BLACK;
        this.f = f;
        this.s = s;
    }

    //геттеры
    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getW() {
        return this.w;
    }

    public double getH() {
        return this.h;
    }

    //отрисовка объекта
    public void draw(Graphics2D g) {
        g.setColor(Color.red);//задаем цвет объекту Color
        Font font = new Font("Arial", Font.ITALIC, 60);//создаем объект класса фонт (передаем в конструктор параметры)
        g.setFont(font);//устанавливаем наш шрифт
        g.drawString("Сложность", 40, 100);//рисуем строчку
        g.drawString("Звук", 40, 250);//рисуем строчку
        g.drawString("Управление", 40, 400);//рисуем строчку

        g.drawImage(new ImageIcon(s).getImage(), (int) x, (int) y, null);//отрисовываем элемент в координатах
        g.setColor(color1);//задаем цвет объекту Color
        Font font1 = new Font("Arial", Font.ITALIC, 20);//создаем объект класса фонт (передаем в конструктор параметры)
        g.setFont(font1);//устанавливаем наш шрифт
        long length = (int) g.getFontMetrics().getStringBounds(f, g).getWidth();//длина надписи в пикселях
        g.drawString(f, (int) (x + w / 2) - (int) (length / 2), (int) y + (int)(h / 3) * 2);//рисуем строчку в центре панели

        g.setColor(Color.red);//задаем цвет объекту Color
        Font font2 = new Font("Arial", Font.ITALIC, 40);//создаем объект класса фонт (передаем в конструктор параметры)
        g.setFont(font2);//устанавливаем наш шрифт
        if (Panel.easy) g.drawString("легко", 1000, 100);//рисуем строчку
        if (Panel.norm) g.drawString("средне", 1000, 100);//рисуем строчку
        if (Panel.hard) g.drawString("сложно", 1000, 100);//рисуем строчку
        if (Panel.aud)g.drawImage(new ImageIcon("image/aud_on.png").getImage(), 1000, 200, null);
        if (!Panel.aud)g.drawImage(new ImageIcon("image/aud_off.png").getImage(), 1000, 200, null);
        if (Panel.control) g.drawString("стандарт", 1000, 400);//рисуем строчку
        if (!Panel.control) g.drawString("пользовательское", 1000, 400);//рисуем строчку
    }
}
