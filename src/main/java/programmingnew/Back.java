package programmingnew;

import javax.swing.*;
import java.awt.*;

/**
 * Задний фон игровой панели
 */
public class Back {
    Image img = new ImageIcon("image/fon.jpg").getImage();//загрузка картинки

    public void draw(Graphics2D g) {//прорисовка в Graphics2D
        Color bacColor = new Color(37, 255, 38);//создаем объект класса цвет
        g.setColor(bacColor);//передаем цвет граф.объекту

        if (Panel.state.equals(Panel.STATES.MENUE)) {
            g.fillRect(0, 0, Panel.WIDTH, Panel.HEIGHT);//рисуем прямоугольную область
        }
        if (Panel.state.equals(Panel.STATES.PLAY)) {
            g.drawImage(img, (int) 0, (int) 0, null);//отрисовываем элемент в координатах
        }
    }
}
