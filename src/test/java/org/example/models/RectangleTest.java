package org.example.models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RectangleTest {

    @Test
    void isOverlapTest() {
        Rectangle rectangle1 = new Rectangle(new Point(1, 1), 1, 1);
        Rectangle otherRectangle = new Rectangle(new Point(2, 2), 1, 1);
        Rectangle thirdRectangle = new Rectangle(new Point(2, 2), 2, 1);
        assertTrue(rectangle1.isOverlap(otherRectangle));
        assertTrue(rectangle1.isOverlap(thirdRectangle));
    }

    @Test
    void isOverlapDistanceTest() {
        Rectangle rectangle1 = new Rectangle(new Point(1, 1), 1, 1);
        Rectangle rectangle2 = new Rectangle(new Point(10, 10), 1, 1);
        assertFalse(rectangle1.isOverlap(rectangle2));
    }

    @Test
    void isOverlapCrossingTest() {
        Rectangle rectangle1 = new Rectangle(new Point(2, 1), 2, 1);
        Rectangle rectangle2 = new Rectangle(new Point(2, 2), 1, 2);
        assertTrue(rectangle1.isOverlap(rectangle2));
    }

    @Test
    void isOverlapNearTest() {
        Rectangle rectangle1 = new Rectangle(new Point(1, 1), 1, 1);
        Rectangle rectangle2 = new Rectangle(new Point(3, 1), 1, 1);
        Rectangle rectangle3 = new Rectangle(new Point(5, 5), 1, 1);
        Rectangle rectangle4 = new Rectangle(new Point(5, 3), 1, 1);
        Rectangle rectangle5 = new Rectangle(new Point(5, 3), 2, 1);
        assertFalse(rectangle1.isOverlap(rectangle2));
        assertFalse(rectangle3.isOverlap(rectangle4));
        assertTrue(rectangle3.isOverlap(rectangle5));
    }

    @Test
    void isOverlapPlusTest() {
        Rectangle rectangle1 = new Rectangle(new Point(2, 1), 3, 1);
        Rectangle rectangle2 = new Rectangle(new Point(1, 2), 1, 3);
        assertTrue(rectangle1.isOverlap(rectangle2));
    }

    @Test
    void isOverlapTouchTest() {
        Rectangle rectangle1 = new Rectangle(new Point(2, 1), 1, 1);
        Rectangle rectangle2 = new Rectangle(new Point(1, 2), 1, 1);
        assertTrue(rectangle1.isOverlap(rectangle2));
    }

    @Test
    void checkOverlapBagsTest() {
        Rectangle rectangle1 = new Rectangle(new Point(2, 2), 4, 1);
        Rectangle rectangle2 = new Rectangle(new Point(2, 7), 1, 3);
        assertFalse(rectangle1.isOverlap(rectangle2));
    }

    @Test
    void checkContainsPointTest() {
        Rectangle rectangle = new Rectangle(new Point(2, 2), 4, 1);
        Point point1 = new Point(2, 3);
        Point point2 = new Point(8, 8);
        Point point3 = new Point(3, 3);
        Point point4 = new Point(1, 3);
        Point point5 = new Point(2, 1);
        Point point6 = new Point(2, 6);
        assertTrue(rectangle.containsPoint(point1)); //внутри
        assertFalse(rectangle.containsPoint(point2)); //далеко
        assertFalse(rectangle.containsPoint(point3)); //зона отчуждения, снизу
        assertFalse(rectangle.containsPoint(point4)); //зона отчуждения, сверху
        assertFalse(rectangle.containsPoint(point5)); //зона отчуждения, слева
        assertFalse(rectangle.containsPoint(point6)); //зона отчуждения, справа
    }
}