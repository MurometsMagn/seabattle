package org.example.models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PointTest {

    @Test
    void pointToStringTest() {
        Point point = new Point(2, 2);
        assertEquals("B2", point.toString());
    }

}